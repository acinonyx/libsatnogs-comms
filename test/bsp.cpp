#include "main.h"
#include <at86rf215.h>
#include <cstdint>
#include <stm32h743xx.h>

extern SPI_HandleTypeDef hspi4;

int
at86rf215_set_rstn(uint8_t enable)
{
  HAL_GPIO_WritePin(RF_RST_GPIO_Port, RF_RST_Pin,
                    enable ? GPIO_PIN_SET : GPIO_PIN_RESET);
  return AT86RF215_OK;
}

int
at86rf215_set_seln(uint8_t enable)
{
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, enable ? GPIO_PIN_SET : GPIO_PIN_RESET);
  return AT86RF215_OK;
}

void
at86rf215_delay_us(uint32_t us)
{
  HAL_Delay(us);
}

int
at86rf215_spi_read(uint8_t *out, const uint8_t *in, size_t len)
{
  return HAL_SPI_TransmitReceive(&hspi4, (uint8_t *)in, out, len, 1000);
}

int
at86rf215_spi_write(const uint8_t *in, size_t len)
{
  return HAL_SPI_Transmit(&hspi4, (uint8_t *)in, len, 1000);
}

int
at86rf215_irq_enable(uint8_t enable)
{
  if (enable) {
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
  } else {
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
  }
  return AT86RF215_OK;
}
