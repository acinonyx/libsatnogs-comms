/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND24_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND24_HPP_

#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

class rf_frontend24 : public rf_frontend
{
public:
  rf_frontend24();
  virtual ~rf_frontend24();
};

} /* namespace satnogs::comms */

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND09_HPP_ */
