/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_F2972_HPP_
#define INCLUDE_SATNOGS_COMMS_F2972_HPP_

#include "stm32h7xx_hal.h"
#include <etl/string.h>

namespace satnogs::comms
{

/**
   * Class for controlling the F2972 RF-switch
   */
class f2972
{
public:
  enum class port : uint8_t
  {
    RF1,
    RF2
  };

  f2972(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
        GPIO_TypeDef *ctl_port, uint16_t ctl_pin);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_port(port p);

  port
  active_port() const;

private:
  etl::string<32> m_name;
  bool            m_enabled;
  GPIO_TypeDef   *m_en_port;
  uint16_t        m_en_pin;
  GPIO_TypeDef   *m_ctl_port;
  uint16_t        m_ctl_pin;
  port            m_port;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_F2972_HPP_ */
