/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_FPF270X_HPP_
#define INCLUDE_SATNOGS_COMMS_FPF270X_HPP_

#include "stm32h7xx_hal.h"
#include <etl/string.h>

namespace satnogs::comms
{

class fpf270x
{
public:
  fpf270x(const etl::istring &name, GPIO_TypeDef *on_port, uint16_t on_pin,
          GPIO_TypeDef *pgood_port = nullptr, uint16_t pgood_pin = 0,
          GPIO_TypeDef *flagb_port = nullptr, uint16_t flagb_pin = 0);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  bool
  pgood() const;

  bool
  flagb() const;

private:
  etl::string<32> m_name;
  bool            m_enabled;
  GPIO_TypeDef   *m_on_port;
  uint16_t        m_on_pin;
  GPIO_TypeDef   *m_pgood_port;
  uint16_t        m_pgood_pin;
  GPIO_TypeDef   *m_flagb_port;
  uint16_t        m_flagb_pin;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_FPF270X_HPP_ */
