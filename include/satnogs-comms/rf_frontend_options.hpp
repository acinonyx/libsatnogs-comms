/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND_OPTIONS_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND_OPTIONS_HPP_

#include <etl/vector.h>
#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/fpf270x.hpp>
#include <satnogs-comms/lna.hpp>
#include <utility>

namespace satnogs::comms
{

class rf_frontend_options
{
public:
  rf_frontend_options();

  void
  set_lna0(const lna &l);

  void
  set_lna1(const lna &l);

  void
  set_agc(const ad8318 &agc);

  void
  set_tx_freq(float start, float stop);

  void
  set_tx_freq(const std::pair<float, float> range);

  void
  set_rx_freq(float start, float stop);

  void
  set_rx_freq(const std::pair<float, float> range);

  void
  set_freq(const std::pair<float, float> rx_range,
           const std::pair<float, float> tx_range);

  void
  set_rx_rf_sw(const f2972 &f);

  void
  set_rx_load_sw(const fpf270x &lsw);

  void
  set_tx_load_sw(const fpf270x &lsw);

private:
  friend class rf_frontend09;
  friend class rf_frontend24;
  friend class rf_frontend;

  ad8318                  agc;
  fpf270x                 rx_load_sw;
  fpf270x                 tx_load_sw;
  lna                     lna0;
  lna                     lna1;
  f2972                   filt_sel_sw;
  std::pair<float, float> rx_freq_range;
  std::pair<float, float> tx_freq_range;
};

class rf_frontend_options_builder
{
public:
  rf_frontend_options_builder() {}

  rf_frontend_options_builder &
  set_freq(const std::pair<float, float> &rx_range,
           const std::pair<float, float> &tx_range);

  rf_frontend_options_builder &
  set_lna(const lna &lna0, const lna &lna1);

  rf_frontend_options_builder &
  set_lna(const lna &lna0);

  rf_frontend_options_builder &
  set_agc(const ad8318 &agc);

  rf_frontend_options_builder &
  set_load_switches(const fpf270x &rx_lsw, const fpf270x &tx_lsw);

  rf_frontend_options_builder &
  set_rx_filter_swicth(const f2972 &rf_sw);

  rf_frontend_options
  finalize();

private:
  rf_frontend_options opts;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND_OPTIONS_HPP_ */
