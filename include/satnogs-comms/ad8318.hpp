/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_AD8318_HPP_
#define INCLUDE_SATNOGS_COMMS_AD8318_HPP_

#include "stm32h7xx_hal.h"
#include <etl/string.h>

namespace satnogs::comms
{

class ad8318
{
public:
  ad8318(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
         DAC_HandleTypeDef *vset_dac, uint32_t vset_dac_channel,
         ADC_HandleTypeDef *vout_adc, uint32_t vout_channel,
         ADC_HandleTypeDef *temp_adc, uint32_t temp_channel);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  void
  toggle();

  bool
  enabled() const;

  float
  temperature() const;

  void
  vset(uint8_t data_volt);

  float
  vout() const;

  void
  set_vref_adc(float);

private:
  etl::string<32>    m_name;
  bool               m_enabled;
  GPIO_TypeDef      *m_en_port;
  uint16_t           m_en_pin;
  DAC_HandleTypeDef *m_vset_dac;
  uint32_t           m_vset_dac_channel;
  ADC_HandleTypeDef *m_vout_adc;
  uint32_t           m_vout_channel;
  ADC_HandleTypeDef *m_temp_adc;
  uint32_t           m_temp_channel;
  float              m_vref_adc = 3.3f;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_AD8318_HPP_ */
