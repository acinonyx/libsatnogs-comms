/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_LNA_HPP_
#define INCLUDE_SATNOGS_COMMS_LNA_HPP_

#include "stm32h7xx_hal.h"
#include <etl/string.h>

namespace satnogs::comms
{

class lna
{
public:
  lna(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
      bool invert = false);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  void
  toggle();

  bool
  enabled() const;

protected:
  bool          m_enabled;
  GPIO_TypeDef *m_port;
  uint16_t      m_pin;
  bool          m_invert;

private:
  etl::string<32> m_name;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_LNA_HPP_ */
