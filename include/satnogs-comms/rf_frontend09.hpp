/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND09_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND09_HPP_

#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/lna.hpp>
#include <satnogs-comms/rf_frontend.hpp>
#include <satnogs-comms/rf_frontend_options.hpp>

namespace satnogs::comms
{

class rf_frontend09 : public rf_frontend
{
public:
  rf_frontend09(const rf_frontend_options &x);

  void
  enable(bool set = true);

  void
  set_dir(dir d);

  void
  set_rx_hw_filter(filter f);

  void
  set_agc_targer_level(float tgt);

  float
  get_agc_gain();

  float
  get_agc_temp();

  filter
  get_rx_hw_filter() const;

private:
  ad8318     m_agc;
  f2972      m_filt_sel;
  comms::lna m_lna;
};

} /* namespace satnogs::comms */

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND09_HPP_ */
