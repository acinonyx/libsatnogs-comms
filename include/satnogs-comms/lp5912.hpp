/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_LP5912_HPP_
#define INCLUDE_SATNOGS_COMMS_LP5912_HPP_

#include "stm32h7xx_hal.h"
#include <etl/string.h>

namespace satnogs::comms
{

class lp5912
{
public:
  lp5912(const etl::istring &name, GPIO_TypeDef *on_port, uint16_t on_pin,
         GPIO_TypeDef *pgood_port = nullptr, uint16_t pgood_pin = 0);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  bool
  pgood() const;

private:
  const etl::string<32> m_name;
  bool                  m_enabled;
  GPIO_TypeDef *const   m_on_port;
  const uint16_t        m_on_pin;
  GPIO_TypeDef *const   m_pgood_port;
  const uint16_t        m_pgood_pin;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_LP5912_HPP_ */
