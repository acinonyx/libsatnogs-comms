/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_

#include <at86rf215.h>
#include <etl/string.h>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/fpf270x.hpp>
#include <satnogs-comms/rf_frontend_options.hpp>
#include <stm32h743xx.h>
#include <utility>

namespace satnogs::comms
{

class rf_frontend_at86rf125_exception : public exception
{
public:
  rf_frontend_at86rf125_exception(string_type file_name, numeric_type line,
                                  int err)
      : exception(ETL_ERROR_TEXT("at86rf215 error", "at86err"), file_name, line,
                  err)
  {
  }
};

class rf_frontend_inval_param_exception : public exception
{
public:
  rf_frontend_inval_param_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("rf_frontend invalid param", "rfinvalparam"),
                  file_name, line)
  {
  }
};

class rf_frontend
{
public:
  /**
     * Operational mode
     */
  enum class op_mode
  {
    BASEBAND, /**< The baseband core is active */
    IQ        /**< The IQ functionality is active */
  };

  /**
     * Direction / interface
     */
  enum class dir
  {
    RX, /**< RX interface */
    TX  /**< TX interface */
  };

  /**
   * Interface type of the RF frontend
   */
  enum class iface
  {
    UHF,  /**< UHF */
    SBAND /**< S-BAND */
  };

  enum class filter
  {
    WIDE,
    NARROW
  };

  rf_frontend(const etl::istring &name, const rf_frontend_options &x,
              iface interface);

  const etl::istring &
  name() const;

  /**
   * Enable/Disable the RF frontend and its associated components
   * @param set true to enable, false to disable
   */
  virtual void
  enable(bool set = true) = 0;

  bool
  enabled() const;

  void
  set_mode(rf_frontend::op_mode m);

  rf_frontend::op_mode
  mode() const;

  virtual void
  set_dir(dir d) = 0;

  virtual dir
  get_dir() const;

  virtual void
  set_rx_hw_filter(filter f) = 0;

  virtual void
  set_agc_targer_level(float tgt) = 0;

  virtual float
  get_agc_gain() = 0;

  virtual float
  get_agc_temp() = 0;

  virtual filter
  get_rx_hw_filter() const = 0;

protected:
  std::pair<float, float> m_rx_freq_range;
  std::pair<float, float> m_tx_freq_range;
  iface                   m_iface;
  at86rf215_radio_t       m_radio;
  bool                    m_enabled;
  rf_frontend::op_mode    m_mode;
  at86rf215               m_at86;
  fpf270x                 m_rx_load_sw;
  fpf270x                 m_tx_load_sw;
  dir                     m_dir;

  void
  at86rf215_enable(bool enable = true);

private:
  const etl::string<32> m_name;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_ */
