/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_
#define INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_

#include <etl/exception.h>

namespace satnogs::comms
{

class exception : public etl::exception
{
public:
  exception(const char *reason, const char *file, int lineno)
      : etl::exception(reason, file, lineno), m_has_errno(false), m_err(0)
  {
  }

  exception(const char *reason, const char *file, int lineno, int err)
      : etl::exception(reason, file, lineno), m_has_errno(true), m_err(err)
  {
  }

  /**
   *
   * @return the error number registered with the exception. If none was available
   * this method returns 0.
   */
  int
  err() const
  {
    return m_err;
  }

  /**
   *
   * @return true if the exception has an error code registered, false otherwise
   */
  bool
  has_errno() const
  {
    return m_has_errno;
  }

private:
  const bool m_has_errno;
  const int  m_err;
};

} /* namespace satnogs::comms */

#endif /* INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_ */
