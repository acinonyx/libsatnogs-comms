/* SPDX-License-Identifier: GPL-3.0 */

#ifndef INCLUDE_SATNOGS_COMMS_ERROR_HPP_
#define INCLUDE_SATNOGS_COMMS_ERROR_HPP_

#define ETL_LOG_ERRORS       1
#define ETL_THROW_EXCEPTIONS 1

#include <etl/error_handler.h>

#if defined(ETL_NO_CHECKS)
#error "ETL_NO_CHECKS option is not allowed for libsatnogs-comms"
#else
#define SATNOGS_COMMS_ASSERT_ERROR_CODE(err, e)                                \
  {                                                                            \
    if ((err)) {                                                               \
      etl::error_handler::error((e(__FILE__, __LINE__, err)));                 \
      throw((e(__FILE__, __LINE__, err)));                                     \
    }                                                                          \
  }

#define SATNOGS_COMMS_ERROR(e)                                                 \
  {                                                                            \
    etl::error_handler::error((e(__FILE__, __LINE__)));                        \
    throw((e(__FILE__, __LINE__)));                                            \
  }
#endif

#endif /* INCLUDE_SATNOGS_COMMS_ERROR_HPP_ */
