# SatNOGS COMMS Control Library
SatNOGS COMMS Control Library is an interface library that controls the
[SatNOGS COMMS](https://gitlab.com/librespacefoundation/satnogs-comms) transceiver.
This library relies only on the HAL, allowing users of the
[SatNOGS COMMS](https://gitlab.com/librespacefoundation/satnogs-comms) to build
their own firmware without any extra effort.
The official software of the transceiver utilizing this library and FreeRTOS
can be found at the
[SatNOGS COMMS Software MCU](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu) repository.


## Development Guide
SatNOGS COMMS Control Library is independent of any kind of IDE.
You can use the development environment of your choice.

The codebase contains also a standalone and fully functional test firmware,
under the `test` directory, that can be flashed directly to the board using openOCD.

### Requirements
* CMake (>= 3.21)
* clang-format (>= 13.0)

#### Optional requirements
* openOCD (>= 0.11) (for testing)

### Coding Style
For the C and C++ code, we use **LLVM** style.
Use `clang-format` and the options file `.clang-format` to
adapt to the styling.

At the root directory of the project there is the `clang-format` options
file `.clang-format` containing the proper configuration.
Developers can import this configuration to their favorite editor.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you import on your editor the coding style rules.

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2021 [Libre Space Foundation](https://libre.space).

## Attribution
The CMake build system is based on [stm32-cmake](https://github.com/ObKo/stm32-cmake).
