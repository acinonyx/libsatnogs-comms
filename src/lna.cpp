/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/lna.hpp>

namespace satnogs::comms
{

/**
 * Creates a generic LNA handler
 * @param name the name of the LNA
 * @param en_port the HAL GPIO port to enable/disable the LNA
 * @param en_pin the HAL GPIO pin to enable/disable the LNA
 * @param invert set to true if the logic on the GPIO pin is inverted
 */
lna::lna(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
         bool invert)
    : m_enabled(false),
      m_port(en_port),
      m_pin(en_pin),
      m_invert(invert),
      m_name(name)
{
  enable(false);
}

const etl::istring &
lna::name() const
{
  return m_name;
}

/**
 * Enable/disable the LNA
 * @param set set to true to enable the LNA, false to disable.
 * If the logic is inverted, it is handled automatically internally
 */
void
lna::enable(bool set)
{
  HAL_GPIO_WritePin(m_port, m_pin,
                    (set && !m_invert) ? GPIO_PIN_SET : GPIO_PIN_RESET);
  m_enabled = set;
}

/**
 * Toggles the state of the LNA
 */
void
lna::toggle()
{
  enable(!m_enabled);
}

/**
 *
 * @return true of the LNA is enabled, false otherwise
 */
bool
lna::enabled() const
{
  return m_enabled;
}

} // namespace satnogs::comms
