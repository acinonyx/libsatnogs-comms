/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/ad8318.hpp>

namespace satnogs::comms
{

ad8318::ad8318(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
               DAC_HandleTypeDef *vset_dac, uint32_t vset_dac_channel,
               ADC_HandleTypeDef *vout_adc, uint32_t vout_channel,
               ADC_HandleTypeDef *temp_adc, uint32_t temp_channel)
    : m_name(name),
      m_en_port(en_port),
      m_en_pin(en_pin),
      m_vset_dac(vset_dac),
      m_vset_dac_channel(vset_dac_channel),
      m_vout_adc(vout_adc),
      m_vout_channel(vout_channel),
      m_temp_adc(temp_adc),
      m_temp_channel(temp_channel)
{
}

const etl::istring &
ad8318::name() const
{
  return m_name;
}

/**
 * Enables/Disables the AD8318
 * @param set set to true to enable or false to disable
 */
void
ad8318::enable(bool set)
{
  HAL_GPIO_WritePin(m_en_port, m_en_pin, set ? GPIO_PIN_SET : GPIO_PIN_RESET);
  if (set) {
    HAL_DAC_Start(m_vset_dac, m_vset_dac_channel);
    HAL_DAC_SetValue(m_vset_dac, m_vset_dac_channel, DAC_ALIGN_8B_R, 0);
  } else {
    HAL_DAC_SetValue(m_vset_dac, m_vset_dac_channel, DAC_ALIGN_8B_R, 0);
  }
  m_enabled = set;
}

/**
   * Toggles the state of the AD8318
   */
void
ad8318::toggle()
{
  enable(!m_enabled);
}

/**
 *
 * @return true if the IC is enabled, false otherwise
 */
bool
ad8318::enabled() const
{
  return m_enabled;
}

/**
 * Get the equivalent voltage of AGC temperature
 * @return the volts that needs to convert in temperature after calibration
 */
float
ad8318::temperature() const
{
  // Configure ADC Channel
  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel                = m_temp_channel;
  sConfig.Rank                   = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime           = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.SingleDiff             = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber           = ADC_OFFSET_NONE;
  sConfig.Offset                 = 0;
  sConfig.OffsetSignedSaturation = DISABLE;
  HAL_ADC_ConfigChannel(m_temp_adc, &sConfig);
  HAL_ADC_Start(m_temp_adc);
  HAL_ADC_PollForConversion(m_temp_adc, 100);
  /* clang-format off */
  float value = __LL_ADC_CALC_DATA_TO_VOLTAGE((m_vref_adc * 1000),
                                              (uint32_t)HAL_ADC_GetValue(m_temp_adc),
                                              m_temp_adc->Init.Resolution) / 1000.0;
  /* clang-format on */
  HAL_ADC_Stop(m_vout_adc);
  return value;
}

/**
 * Set the output power level of AGC in digital value of uint8.
 * @param data_volt the value to set in uint8
 */
void
ad8318::vset(uint8_t data_volt)
{
  HAL_DAC_SetValue(m_vset_dac, m_vset_dac_channel, DAC_ALIGN_8B_R, data_volt);
}

/**
 * Get the gain in voltage (0-1.4)Volts that the VGA is setting up.
 * @return the VOUT in volts
 */
float
ad8318::vout() const
{
  // Configure ADC Channel
  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel                = m_vout_channel;
  sConfig.Rank                   = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime           = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.SingleDiff             = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber           = ADC_OFFSET_NONE;
  sConfig.Offset                 = 0;
  sConfig.OffsetSignedSaturation = DISABLE;
  HAL_ADC_ConfigChannel(m_vout_adc, &sConfig);
  HAL_ADC_Start(m_vout_adc);
  HAL_ADC_PollForConversion(m_vout_adc, 100);
  /* clang-format off */
  float value =  __LL_ADC_CALC_DATA_TO_VOLTAGE((m_vref_adc * 1000),
                                               (uint32_t)HAL_ADC_GetValue(m_vout_adc),
                                               m_vout_adc->Init.Resolution) / 1000.0;
  /* clang-format on */
  HAL_ADC_Stop(m_vout_adc);
  return value;
}

/**
 * Sets the reference voltage of ADC in Volts
 * @param volts the value to set in volts
 */
void
ad8318::set_vref_adc(float volts)
{
  // Check the volts if it is valid
  m_vref_adc = volts;
}

} // namespace satnogs::comms
