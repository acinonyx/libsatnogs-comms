/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/rf_frontend09.hpp>

namespace satnogs::comms
{

rf_frontend09::rf_frontend09(const rf_frontend_options &x)
    : rf_frontend(etl::make_string("rf09"), x, iface::UHF),
      m_agc(x.agc),
      m_filt_sel(x.filt_sel_sw),
      m_lna(x.lna0)
{
  m_agc.enable(false);
  m_filt_sel.enable(false);
  m_lna.enable(false);
}

/**
 * Enables the basic subsystems of the RF frontend. Subsystems that are
 * used for a specific direction (RX or TX) are explicitly disabled to reduce
 * power consumption.
 *
 * Users may have to call the set_dir() method to enable the corresponding
 * subsystems such as PA or LNA.
 *
 * @param set true to enable, false to disable.
 */
void
rf_frontend09::enable(bool set)
{
  at86rf215_enable(set);

  /*
   * Explicit set to false the subsystems that depends on the direction of
   * the signal
   */
  m_filt_sel.enable(false);
  m_rx_load_sw.enable(false);
  m_tx_load_sw.enable(false);
  m_agc.enable(false);
  m_lna.enable(false);
  m_enabled = set;
}

/**
 * Sets the direction of the RF frontend, enables the corresponding components
 * and disables the components that are not needed to reduce consumption
 * @param d the direction
 */
void
rf_frontend09::set_dir(dir d)
{
  switch (d) {
  case dir::RX:
    m_tx_load_sw.enable(false);
    m_rx_load_sw.enable(true);
    m_filt_sel.enable(true);
    m_agc.enable(true);
    m_lna.enable(true);
    break;
  case dir::TX:
    m_tx_load_sw.enable(true);
    m_rx_load_sw.enable(false);
    m_filt_sel.enable(false);
    m_agc.enable(false);
    m_lna.enable(false);
    break;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
    break;
  }
  m_dir = d;
}

/**
 * Sets the RX hardware filter path
 * @param f the RF filter path (filter::NARROW or filter::WIDE)
 */
void
rf_frontend09::set_rx_hw_filter(filter f)
{
  switch (f) {
  case filter::WIDE:
    m_filt_sel.set_port(f2972::port::RF1);
    break;
  case filter::NARROW:
    m_filt_sel.set_port(f2972::port::RF2);
    break;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
    break;
  }
}

/**
 * Sets the target level of AGC, slope and intercept of the linear regression equation might be
 * given by the user
 * @param tgt is the target level in the AGC in dBm (float),-20 < tgt < -60
 */
void
rf_frontend09::set_agc_targer_level(float tgt)
{
  m_agc.vset((uint8_t)(-1.8 * tgt + 47.4));
}

/**
 * Gets the gain of AGC amplifier in dB, slope and intercept of the linear regression equation
 * might be given by the user
 * @return the gain in dB
 */
float
rf_frontend09::get_agc_gain()
{
  return 0.05 * (m_agc.vout() * 1000.0) - 41.67;
}

/**
 * Gets the temperature of AGC controller in °C, slope is given by datasheet of controller the
 * intercept could be calculated by the user
 * @return the temperature in °C
 */
float
rf_frontend09::get_agc_temp()
{
  return 0.5 * (m_agc.temperature() * 1000.0) - 313.0;
}

/**
 *
 * @return the selected RX hardware filter path
 */
rf_frontend09::filter
rf_frontend09::get_rx_hw_filter() const
{
  if (m_filt_sel.active_port() == f2972::port::RF1) {
    return filter::WIDE;
  }
  return filter::NARROW;
}

} // namespace satnogs::comms
