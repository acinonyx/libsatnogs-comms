/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/rf_frontend_options.hpp>

namespace satnogs::comms
{

rf_frontend_options::rf_frontend_options()
    : agc(etl::make_string("INVALID"), nullptr, 0, nullptr, 0, nullptr, 0,
          nullptr, 0),
      rx_load_sw(etl::make_string("INVALID"), nullptr, 0),
      tx_load_sw(etl::make_string("INVALID"), nullptr, 0),
      lna0(etl::make_string("INVALID"), nullptr, 0, false),
      lna1(etl::make_string("INVALID"), nullptr, 0, false),
      filt_sel_sw(etl::make_string("INVALID"), nullptr, 0, nullptr, 0)
{
}

void
rf_frontend_options::set_lna0(const lna &l)
{
  lna0 = l;
}

void
rf_frontend_options::set_lna1(const lna &l)
{
  lna1 = l;
}

void
rf_frontend_options::set_agc(const ad8318 &agc)
{
  this->agc = agc;
}

void
rf_frontend_options::set_tx_freq(float start, float stop)
{
  set_tx_freq({start, stop});
}

void
rf_frontend_options::set_tx_freq(const std::pair<float, float> range)
{
  tx_freq_range = range;
}

void
rf_frontend_options::set_rx_freq(float start, float stop)
{
  set_rx_freq({start, stop});
}

void
rf_frontend_options::set_rx_freq(const std::pair<float, float> range)
{
  rx_freq_range = range;
}

void
rf_frontend_options::set_freq(const std::pair<float, float> rx_range,
                              const std::pair<float, float> tx_range)
{
  set_rx_freq(rx_range);
  set_tx_freq(tx_range);
}

void
rf_frontend_options::set_rx_rf_sw(const f2972 &f)
{
}

void
rf_frontend_options::set_rx_load_sw(const fpf270x &lsw)
{
  rx_load_sw = lsw;
}

void
rf_frontend_options::set_tx_load_sw(const fpf270x &lsw)
{
  tx_load_sw = lsw;
}

/**
 * Sets permicable frequency ranges for RX and TX
 * @param rx_range the RX frequency range
 * @param tx_range the TX frequency range
 * @return @return *this. By exploiting the method chaining the Named Parameter
 * Idiom can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_freq(const std::pair<float, float> &rx_range,
                                      const std::pair<float, float> &tx_range)
{
  opts.set_freq(rx_range, tx_range);
  return *this;
}

/**
 * Sets the LNAs for those interfaces supporting two of them.
 * @param lna0 the first LNA stage, that is closer to the antenna
 * @param lna1 the second LNA stage
 * @return *this. By exploiting the method chaining the Named Parameter Idiom
 * can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_lna(const lna &lna0, const lna &lna1)
{
  opts.set_lna0(lna0);
  opts.set_lna1(lna1);
  return *this;
}

/**
 * Sets the LNA for those interfaces that have a single LNA stage.
 * @param lna0
 * @return @return *this. By exploiting the method chaining the Named Parameter Idiom
 * can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_lna(const lna &lna0)
{
  return *this;
  opts.set_lna0(lna0);
}

/**
 * Sets the AGC
 * @param agc the AGC object
 * @return *this. By exploiting the method chaining the Named Parameter Idiom
 * can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_agc(const ad8318 &agc)
{
  opts.set_agc(agc);
  return *this;
}

/**
 * Sets the load swicthes for RX and TX
 * @param rx_lsw the RX load switch
 * @param tx_lsw the TX load swicth
 * @return *this. By exploiting the method chaining the Named Parameter Idiom
 * can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_load_switches(const fpf270x &rx_lsw,
                                               const fpf270x &tx_lsw)
{
  opts.set_rx_load_sw(rx_lsw);
  opts.set_tx_load_sw(tx_lsw);
  return *this;
}

/**
 * Sets the RF switch that controls the RX path, through a narrow or wide
 * filter
 * @param rf_sw the RF switch
 * @return *this. By exploiting the method chaining the Named Parameter Idiom
 * can be used
 */
rf_frontend_options_builder &
rf_frontend_options_builder::set_rx_filter_swicth(const f2972 &rf_sw)
{
  opts.set_rx_rf_sw(rf_sw);
  return *this;
}

rf_frontend_options
rf_frontend_options_builder::finalize()
{
  return opts;
}

} // namespace satnogs::comms
