/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/f2972.hpp>

namespace satnogs::comms
{

/**
 * Creates an object for controlling the F2972 RF-switch. If the initialization
 * is successful the switch is set to a disabled state. In this state port ports
 * are terminated. However, if the switch is enabled, the first selected port
 * will be the RF1, unless the user specifies another one.
 *
 * @param name a descriptive name of the switch
 * @param en_port the HAL GPIO port of the EN pin
 * @param en_pin the HAL GPIO pin of the EN pin
 * @param ctl_port the HAL GPIO port of the CTL pin
 * @param ctl_pin the HAL GPIO pin of the CTL pin
 */
f2972::f2972(const etl::istring &name, GPIO_TypeDef *en_port, uint16_t en_pin,
             GPIO_TypeDef *ctl_port, uint16_t ctl_pin)
    : m_name(name),
      m_enabled(false),
      m_en_port(en_port),
      m_en_pin(en_pin),
      m_ctl_port(ctl_port),
      m_ctl_pin(ctl_pin)
{
  enable(false);
  set_port(m_port);
}

/**
 *
 * @return the name of the switch
 */
const etl::istring &
f2972::name() const
{
  return m_name;
}

/**
 * Enables/Disables the RF-switch
 * @param set if set to true the switch is enabled, otherwise it is disabled
 */
void
f2972::enable(bool set)
{
  HAL_GPIO_WritePin(m_en_port, m_en_pin, set ? GPIO_PIN_SET : GPIO_PIN_RESET);
  m_enabled = set;
}

/**
 *
 * @return true if the switch is enabled, false otherwise
 */
bool
f2972::enabled() const
{
  return m_enabled;
}

/**
 * Selects the RF-switch port
 * @param p the port to connect with the RFC
 */
void
f2972::set_port(port p)
{
  switch (p) {
  case port::RF1:
    HAL_GPIO_WritePin(m_ctl_port, m_ctl_pin, GPIO_PIN_RESET);
    break;
  case port::RF2:
    HAL_GPIO_WritePin(m_ctl_port, m_ctl_pin, GPIO_PIN_SET);
    break;
  }
  m_port = p;
}

/**
 *
 * @return the selected port. If the switch is disabled, the return value does
 * not makes sense. Check the datasheet for more details
 */
f2972::port
f2972::active_port() const
{
  return m_port;
}

} // namespace satnogs::comms
