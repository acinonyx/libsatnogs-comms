/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/lp5912.hpp>

namespace satnogs::comms
{

lp5912::lp5912(const etl::istring &name, GPIO_TypeDef *on_port, uint16_t on_pin,
               GPIO_TypeDef *pgood_port, uint16_t pgood_pin)
    : m_name(name),
      m_enabled(false),
      m_on_port(on_port),
      m_on_pin(on_pin),
      m_pgood_port(pgood_port),
      m_pgood_pin(pgood_pin)
{
  enable(false);
}

const etl::istring &
lp5912::name() const
{
  return m_name;
}

/**
 * Enables/Disables the LP5912
 * @param set set to true to enable or false to disable
 */
void
lp5912::enable(bool set)
{
  /* Active High! */
  HAL_GPIO_WritePin(m_on_port, m_on_pin, set ? GPIO_PIN_SET : GPIO_PIN_RESET);
  m_enabled = set;
}

/**
 * Return the power state of LP5912
 * @return true if the IC is ON, false otherwise
 */
bool
lp5912::enabled() const
{
  return m_enabled;
}

/**
 * Return the power-good indicator
 * @return true if the IC is working properly, false otherwise
 */
bool
lp5912::pgood() const
{
  if (m_pgood_port) {
    return HAL_GPIO_ReadPin(m_pgood_port, m_pgood_pin) == GPIO_PIN_SET;
  }
  /* If the pin was not available return false */
  return false;
}

} // namespace satnogs::comms
