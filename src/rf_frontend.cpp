/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

rf_frontend::rf_frontend(const etl::istring &name, const rf_frontend_options &x,
                         iface interface)
    : m_rx_freq_range(x.rx_freq_range),
      m_tx_freq_range(x.tx_freq_range),
      m_iface(interface),
      m_radio(interface == iface::UHF ? AT86RF215_RF09 : AT86RF215_RF24),
      m_enabled(false),
      m_mode(op_mode::BASEBAND),
      m_rx_load_sw(x.rx_load_sw),
      m_tx_load_sw(x.tx_load_sw),
      m_name(name)
{
  at86rf215_enable(false);
  m_rx_load_sw.enable(false);
  m_tx_load_sw.enable(false);
}

const etl::istring &
rf_frontend::name() const
{
  return m_name;
}

/**
 *
 * @return true if the RF frontend is enabled and fully operational, false
 * otherwise
 */
bool
rf_frontend::enabled() const
{
  return m_enabled;
}

/**
 * Sets the operational mode of the RF frontend. Any call to this method
 * sets the AT86RF215 for both TX and RX to TRXOFF state.
 * @param m the operational mode
 */
void
rf_frontend::set_mode(rf_frontend::op_mode m)
{
  int ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, m_radio);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend_at86rf125_exception);
  switch (m) {
  case rf_frontend::op_mode::BASEBAND:
    ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
    break;
  case rf_frontend::op_mode::IQ:
    ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_RF);
    break;
  }
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend_at86rf125_exception);
}

/**
 *
 * @return the operational mode of the RF frontend
 */
rf_frontend::op_mode
rf_frontend::mode() const
{
  return m_mode;
}

/**
 *
 * @return the direction of the RF interface that is currently configured
 */
rf_frontend::dir
rf_frontend::get_dir() const
{
  return m_dir;
}

void
rf_frontend::at86rf215_enable(bool enable)
{
  /* In case the user needs to disable the IC, just de-assert the RSTN pin */
  if (enable == false) {
    at86rf215_set_rstn(0);
    return;
  }

  m_at86.clk_drv      = AT86RF215_RF_DRVCLKO8;
  m_at86.clko_os      = AT86RF215_RF_CLKO_26_MHZ;
  m_at86.xo_trim      = 0;
  m_at86.xo_fs        = 0;
  m_at86.irqp         = 0;
  m_at86.irqmm        = 0;
  m_at86.pad_drv      = AT86RF215_RF_DRV4;
  m_at86.rf_femode_09 = AT86RF215_RF_FEMODE2;
  m_at86.rf_femode_24 = AT86RF215_RF_FEMODE2;

  int ret = at86rf215_init(&m_at86);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend_at86rf125_exception);
  ret = at86rf215_transceiver_reset(&m_at86, m_radio);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend_at86rf125_exception);
  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, m_radio);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend_at86rf125_exception);
}

} // namespace satnogs::comms
