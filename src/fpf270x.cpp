/* SPDX-License-Identifier: GPL-3.0 */

#include <satnogs-comms/fpf270x.hpp>

namespace satnogs::comms
{

fpf270x::fpf270x(const etl::istring &name, GPIO_TypeDef *on_port,
                 uint16_t on_pin, GPIO_TypeDef *pgood_port, uint16_t pgood_pin,
                 GPIO_TypeDef *flagb_port, uint16_t flagb_pin)
    : m_name(name),
      m_enabled(false),
      m_on_port(on_port),
      m_on_pin(on_pin),
      m_pgood_port(pgood_port),
      m_pgood_pin(pgood_pin),
      m_flagb_port(flagb_port),
      m_flagb_pin(flagb_pin)
{
  enable(false);
}

const etl::istring &
fpf270x::name() const
{
  return m_name;
}

void
fpf270x::enable(bool set)
{
  /* Active Low! */
  HAL_GPIO_WritePin(m_on_port, m_on_pin, set ? GPIO_PIN_RESET : GPIO_PIN_SET);
  m_enabled = set;
}

bool
fpf270x::enabled() const
{
  return m_enabled;
}

bool
fpf270x::pgood() const
{
  if (m_pgood_port) {
    return HAL_GPIO_ReadPin(m_pgood_port, m_pgood_pin) == GPIO_PIN_SET;
  }
  /* If the pin was not available return false */
  return false;
}

bool
fpf270x::flagb() const
{
  if (m_flagb_port) {
    /* Active Low! */
    return HAL_GPIO_ReadPin(m_flagb_port, m_flagb_pin) == GPIO_PIN_RESET;
  }
  /* If the pin was not available return false */
  return false;
}

} // namespace satnogs::comms
